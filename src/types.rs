pub fn run() {
    //default i32
    let x = 1;

    //default is f64
    let y = 2.5;

    //add explicit type
    let z: i64 = 45454545454;

    //find max size
    println!("Max i32: {}", std::i32::MAX);
    println!("Max i64: {}", std::i64::MAX);

    //Boolean
    let is_active: bool = true;
    let a1 = '\u{1f600}';
    println!("{:?}", (x, y, z, is_active, a1));
}
