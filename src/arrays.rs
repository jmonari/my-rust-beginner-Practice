pub fn run() {
    let mut num: [i32; 5] = [1, 2, 3, 4, 5];
    num.reverse();
    println!("{:?}", num);
    //get single value &U change a value
    num[2] = 20;
    println!("{:?}", num);
    println!("{:?}", num.len());
    println!("{:?}", num[3]);

    //get slice

    let slice: &[i32] = &num[0..2];
    println!("{:?}", slice);
}
