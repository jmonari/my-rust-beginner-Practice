fn greeting(greet: &str, name: &str) {
    println!("{} {}", greet, name);
}

fn add(n1: i32, n2: i32) -> i32 {
    n1 + n2
}

pub fn run() {
    greeting("hello", "World");

    // .bind function values to variables
    let get_sum = add(5, 5);
    println!("{}", get_sum);

    // Closure
    let add_nums = |n1: i32, n2: i32| n1 + n2;
    println!("C sums: {}", add_nums(2, 4));
}
