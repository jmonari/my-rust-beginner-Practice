pub fn run() {
    let mut num: Vec<i32> = vec![1, 2, 3, 4, 5];
    num.reverse();
    println!("{:?}", num);
    //get single value &U change a value
    num[2] = 20;
    println!("{:?}", num);
    println!("{:?}", num.len());
    println!("{:?}", num[3]);

    //get slice
    let slice: &[i32] = &num[0..2];
    println!("{:?}", slice);

    //add onto Vector
    num.push(6);
    num.push(8);
    num.sort();
    num.reverse();
    //pop
    num.pop();
    println!("{:?}", num);

    //loop through vector values

    for x in num.iter() {
        println!("Number: {}", x)
    }
    for x in num.iter_mut() {
        *x *= 2;
        println!("Number: {:?}", x)
    }
}
