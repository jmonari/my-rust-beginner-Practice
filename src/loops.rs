pub fn run() {
    let mut count = 0;
    //fizzbuzz
    loop {
        count += 1;
        if count == 100 {
            break;
        }

        if count % 15 == 0 {
            println!("FizzBuzz");
        } else if count % 3 == 0 {
            println!("Fizz")
        } else if count % 5 == 0 {
            println!("Buzz")
        } else {
            println!("{}", count);
        }
    }

    //for range

    for x in 0..100 {
        if x % 15 == 0 {
            println!("Fizzbuzz");
        } else if x % 3 == 0 {
            println!("Fizz");
        } else if x % 5 == 0 {
            println!("Buzz");
        } else {
            println!("{}", x);
        }
    }
}
