// mod arrays;
// mod print;
// mod strings;
// mod tuples;
// mod types;
// mod vars;
// mod vectors;
// mod conditions;
// mod funcs;
// mod pointers;
// mod loops;
mod structs;
//import and print to console
fn main() {
    // print::run();

    // //Basic Fromatting
    // println!("{} is From {}", "Monari", "Mars");

    // //positional Arguements
    // println!(
    //     "{0} is From {1} and {0} loves {2}",
    //     "Monari", "Mars", "Kenya"
    // );

    // //named arguements
    // println!(
    //     "{name} likes to play {activity}",
    //     name = "John",
    //     activity = "Baseball"
    // );

    // //placeholder traits
    // println!("Binary: {:b} Hex: {:x} Octal: {:o}", 10, 10, 10);

    // //placeholder for debug trait (tuple)
    // println!("{:?}", (12, true, "hello"));

    // //basic math
    // println!("10 + 10 = {}", 10 + 10);

    // //vars
    // vars::run();

    // //types
    // types::run();

    // //strings
    // strings::run();

    // //tuples
    // tuples::run();

    // //arrays
    // arrays::run();

    //vectors
    // vectors::run();

    //conditions
    // conditions::run();
    // loops
    // loops::run();
    //funcs
    // funcs::run();

    // pointers
    // pointers::run();
    //structs
    structs::run();
}
