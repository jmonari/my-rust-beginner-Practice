pub fn run() {
    //immutable
    let hello = "Hello";

    //mutable
    let mut hello2 = String::from("Hello ");

    //get length
    let hello3 = hello2.len();
    println!("{} \n {}", hello, hello3);

    hello2.push('w'); // push a char
    hello2.push_str("orld!"); //push a string
    println!("{}", hello2);

    //empty

    let hello4 = hello2.capacity();
    let hello5 = hello2.is_empty();
    let hello6 = hello2.contains("World!");

    //loop through strings

    println!(
        "Capacity: {} \n Is Empty: {} \n Contains World!:{}",
        hello4, hello5, hello6
    );
    for word in hello2.split_whitespace() {
        println!("{}", word);
    }

    //Create String with Capacity(stack)
    let mut s = String::with_capacity(10);
    s.push('a');
    s.push('b');

    //Asserion Testing
    assert_eq!(2, s.len());
    assert_eq!(10, s.capacity());

    println!("{}", s);
}
