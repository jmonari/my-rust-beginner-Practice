pub fn run() {
    //primitiva array

    let arr1 = [1, 2, 3];
    let arr2 = arr1;

    println!("{:?}", (arr1, arr2));

    let vec = vec![1, 2, 3];
    let vec2 = &vec;

    println!("{:?}", (&vec, vec2));
}
