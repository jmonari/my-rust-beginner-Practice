pub fn run() {
    let name = "Monari";
    let mut age = 25;

    println!("My name is: {} and i am {}", name, age);

    age = 24;
    println!("My name is: {} and i am {}", name, age);

    //Define Const

    const ID: i32 = 001;

    println!("ID: {}", ID);

    //assign multiple variables at once
    let (my_name, my_age) = ("Monari", 24);
    println!("{} is {}", my_name, my_age);
}

// pub fn hello() -> &'static str {
//     "Hello, World!"
// }
