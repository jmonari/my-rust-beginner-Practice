//Traditional struct
struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

//Tuple Struct

struct Colors(u8, u8, u8);

//Person Struct

struct Person {
    first_name: String,
    last_name: String,
}

impl Person {
    //Construct person
    fn new(first: &str, last: &str) -> Person {
        Person {
            first_name: first.to_string(),
            last_name: last.to_string(),
        }
    }
    //Full name
    fn full_name(&self) -> String {
        format!("{} {}", self.first_name, self.last_name)
    }

    //set last name
    fn set_last_name(&mut self, last: &str) {
        self.last_name = last.to_string()
    }
}

pub fn run() {
    let mut c = Color {
        red: 255,
        green: 0,
        blue: 0,
    };
    c.red = 200;
    println!("The color is: {} {} {}", c.red, c.blue, c.green);

    let mut x = Colors(255, 0, 0);
    x.1 = 100;
    println!("The color is: {} {} {}", x.0, x.1, x.2);

    let mut p = Person::new("mary", "doe");
    println!("Person: {}", p.full_name());
    p.set_last_name("Williams");
    println!("Person: {}", p.full_name());
    println!("Person: {} {}", p.first_name, p.last_name);
    println!("Person: {}", p.full_name());
}
